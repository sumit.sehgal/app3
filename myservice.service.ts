import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import {Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  constructor(private firestore:AngularFirestore ,private storage:AngularFireStorage) { }
  downloadURL: Observable<string>;

  createpost(postData: FormData, file:File)
  {
    const image = file[0];
    const filepath = Date.now() + "-" + file[0]["name"];
    const fileRef = this.storage.ref(filepath);
    const task = this.storage.upload(filepath, image);
    fileRef.getDownloadURL().subscribe(url => {
      this.downloadURL = url;
    let newpost = {
      title : postData["title"],
      content : postData["content"],
      cover: this.downloadURL,
      fileref: filepath
    }
  
   this.firestore.collection("posts").add(newpost);
  
    })
  }
}
